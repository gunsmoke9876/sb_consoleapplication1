#ifndef HELPER__H
#define HELPER__H
//This header contains necessary math functions.

using namespace std;

//Quite self-explanatory function.
int SquareOfSum(int a, int b);

//Prints out all odd (isOdd == 1) or even (isOdd == 0) numbers in range [0 .. Limit].
int PrintOddOrEven(int Limit, int isOdd);

//Custom vector class.
class MegaVector
{
private:

    double x = 0.0;
    double y = 0.0;
    double z = 0.0;

public:

    MegaVector()
    {}

    MegaVector(double X, double Y, double Z) : x(X), y(Y), z(Z)
    {}

    void SelfPrint()
    {
        cout << "X = " << x << ", Y = " << y << ", Z = " << z;
    }

    double Length()
    {
        return sqrt(x * x + y * y + z * z);
    }
};

//Output MegaVector.
void PrintVectorInfo(MegaVector* V, string Name);

#endif