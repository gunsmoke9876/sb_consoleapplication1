#include <iostream>
#include "Helper.h"

//Quite self-explanatory function.
int SquareOfSum(int a, int b)
{
	int sum = a + b;
	//Toggle a breakpoint at line below to see the parameters' values.
	return sum * sum;
}

//Prints out all odd (isOdd == 1) or even (isOdd == 0) numbers in range [0 .. Limit].
int PrintOddOrEven(int Limit, int isOdd)
{
    int num_count = 0; //how many numbers were found
    for (int x = isOdd; x <= Limit; x += 2, num_count++) //usage of ',' operator
        cout << x << " ";
    cout << endl;
    return num_count;
}

//Output MegaVector.
void PrintVectorInfo(MegaVector* V, string Name)
{
    cout << Name << " coordinates: ";
    V->SelfPrint();
    cout << "; length = " << V->Length() << endl;
}
