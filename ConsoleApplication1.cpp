// ConsoleApplication1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
//#include <string>
//#include <time.h>
#include <math.h>
#include <ctime> //for randomization


//#include "Helper.h"

using namespace std;



class TPlayer
{
private:
    string Name;
    float Score;
public:
    void SetName(string NewName)
    {
        Name = NewName;
    }
    void SetScore(float NewScore)
    {
        Score = NewScore;
    }
    float GetScore()
    {
        return Score;
    }
    void SelfPrint()
    {
        cout << Score << " - " << Name;
    }
};

class Animal
{
public:
    virtual void Voice()
    {
        cout << "*undefinite noise*\n";
    }
};

class Dog : public Animal
{
public:
    virtual void Voice() override
    {
        cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    virtual void Voice() override
    {
        cout << "Meow!\n";
    }
};

class Mice : public Animal
{
public:
    virtual void Voice() override
    {
        cout << "Squeak!\n";
    }
};

int main(int argc, char * argv[])
{
     
/*  //<===== COLLAPSE THIS COMMENT FOR YOUR COMFORT !!!
    //Or uncomment it to run previous homeworks.
    //In that case do not forget to uncomment also all the #includes above.

    //----------------------- Module 13 homework:
    //Call function, defined in Helper.cpp module.
    std::cout << "Homework 13: Square of sum (8 and 2)\n";
    std::cout << SquareOfSum(8, 2) << std::endl;

    //----------------------- Module 14 homework:
    //Declare any string, output the string, it's size,
    //it's first and last characters.
    std::string hw14{ "Hi! This is the string for homework 14. Have a nice day!" };
    std::cout << "\nHomework 14: string\n";
    std::cout << "Message:\n";
    std::cout << hw14 << std::endl;
    std::cout << "Length: " << hw14.size() << " symbols.\n";
    std::cout << "First symbol is: " << hw14[0] << "\n";
    std::cout << "Last symbol is: " << hw14[hw14.size() - 1] << "\n";
    
    //----------------------- Module 15 homework:
    //Function, that prints out all odd or all even numbers
    //in range 0 .. N
    std::cout << "\nHomework 15: Odd or Even\n";
    std::cout << "Input an integer number N to perform search in range [0 .. N]: ";
    int N; //maximum number for the search
    std::cin >> N;
    int SearchOdd = -1; //whether to search odd (1) or even (0) numbers
    while ((SearchOdd != 0) && (SearchOdd != 1))
    {
        //User must input the variable until he either succeed or die.
        std::cout << "To get all odd numbers type '1', to get all even numbers type '0': ";
        std::cin >> SearchOdd;
    }
    //Run the search function.
    if (PrintOddOrEven(N, SearchOdd))
        std::cout << "Enjoy your numbers!\n";
    else
        std::cout << "No numbers for you this time, sorry!\n";

    //----------------------- Module 16 homework:
    cout << "\nHomework 16: Two dimension array\n";
    
    //User inputs desired size of 2 dimension array.
    int M;
    cout << "Input size of 2D array [1 .. 15]:";
    do
    {
        cin >> M;
    }
    while ((M < 1) || (M > 15));
    
    //Create and fill new array of M x M integers.
    int** Arr = new int*[M];
    for (int row = 0; row < M; row++)
    {
        Arr[row] = new int[M];
        for (int col = 0; col < M; col++)
            //Arr[row][col] = row + col;
            Arr[row][col] = row + col * 2;
    }
    
    //Output the array.
    cout << "Array:" << endl;
    for (int row = 0; row < M; row++)
    {
        for (int col = 0; col < M; col++)
            cout << Arr[row][col] << "\t";
        cout << endl;
    }

    //Calculate and output sum of elements of row number (today's date % N).
    tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    int Today = buf.tm_mday;

    cout << "Today's date is " << Today << endl;

    cout << "Sum of elements in col " << Today << " % " << M << " = " << Today % M << " is ";
    int Sum = 0;
    for (int col = 0; col < M; col++)
        Sum += Arr[Today % M][col];
    cout << Sum << endl;


    //----------------------- Module 17 homework:
    cout << "\nHomework 17: Class of a 3D vector\n";

    //Examples of vectors.
    MegaVector vec1(0.0, -3.0, 4.0);
    MegaVector vec2(1.0, -1.0, -1.0);
    MegaVector zero;
    
    //Output coordinates and length of each vector.
    PrintVectorInfo(&vec1, "vec1");
    PrintVectorInfo(&vec2, "vec2");
    PrintVectorInfo(&zero, "zero vector");
/**/
    //----------------------- Module 18 homework:
    cout << "\nHomework 18: Dynamic array sort\n";
    
    //Input players count.
    int PlayersCount;
    do
    {
        cout << "Number of players: ";
        cin >> PlayersCount;
    } while (PlayersCount <= 0);
    
    
    //Input array of players.
    TPlayer** Player = new TPlayer*[PlayersCount];
    for (int p = 0; p < PlayersCount; p++)
    {
        Player[p] = new TPlayer();
        cout << "Player " << p + 1 << " name: ";
        string name;
        cin >> name;
        Player[p]->SetName(name);
        cout << name << "'s score: ";
        float score;
        cin >> score;
        Player[p]->SetScore(score);
    }

    //Bubble-sort players by score.
    bool swapped = true;        //whether array elements been swapped during current cycle
    TPlayer* temp;              //temporary extra room for swapping
    int end = PlayersCount - 1; //sort's last index
    while (swapped)
    {
        swapped = false;
        for (int p = 0; p < end; p++)
        {
            //Swap condition.
            if (Player[p]->GetScore() < Player[p + 1]->GetScore())
            {
                //Swap.
                temp = Player[p];
                Player[p] = Player[p + 1];
                Player[p + 1] = temp;
                swapped = true;
            }
             
        }
        end--;
    }


    //Output players table.
    cout << "\nPlayer ranking is:\n";
    int rank = 1; //player's position may be != element index, as some players may have equal score
    for (int p = 0; p < PlayersCount; p++)
    {
        if ((p > 0) && (Player[p]->GetScore() < Player[p - 1]->GetScore()))
            rank = p + 1;
        cout << rank << ") ";
        Player[p]->SelfPrint();
        cout << endl;

    }

    //Memory cleanup.
    for (int p = 0; p < PlayersCount; p++)
    {
        delete Player[p];
        Player[p] = NULL;
    }
    delete[] Player;
    Player = NULL;
    
    //----------------------- Module 19 homework:
    cout << "\nHomework 19: Animal generator.\n";

    //Create array of animals.
    int AnimalsCount;
    do
    {
        cout << "How many pets would you like? ";
        cin >> AnimalsCount;
    } while (AnimalsCount <= 0);
    Animal** Pet = new Animal * [AnimalsCount];

    //Fill the array with random animals.
    srand(time(0)); //randomizes the random numbers generator
    for (int a = 0; a < AnimalsCount; a++)
    {
        switch (rand() % 4)
        {
        case 1:
            Pet[a] = new Dog();
            break;
        case 2:
            Pet[a] = new Cat();
            break;
        case 3:
            Pet[a] = new Mice();
            break;
        default:
            Pet[a] = new Animal();
        }
    }

    //Listen to their voices.
    cout << "Your new pets are greeting you! Listen!\n";
    for (int a = 0; a < AnimalsCount; a++)
    {
        cout << a + 1 << ") ";
        Pet[a]->Voice();
    }

    //Memory cleanup.
    for (int a = 0; a < AnimalsCount; a++)
    {
        delete Pet[a];
        Pet[a] = NULL;
    }
    delete[] Pet;
    Pet = NULL;

    return 0;
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
